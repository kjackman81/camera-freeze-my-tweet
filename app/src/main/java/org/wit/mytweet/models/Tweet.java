package org.wit.mytweet.models;


public class Tweet {

    /**
     * variables used during the implementation of class
     */
    public String message;
    public String createdAt;
    public String _id;
    //public String blogger;
    public User blogger;
    public String photo;

    /**
     * constructor used for instantiating, with parameters
     * assigns id and date
     */
    public Tweet() {

       // photo = "photo";
    }



}
