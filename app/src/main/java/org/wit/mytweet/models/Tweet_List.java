package org.wit.mytweet.models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.sqlite.DbHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wit.mytweet.helpers.LogHelpers.info;


public class Tweet_List {

    /**
     * variables used  to implement class
     */
    MyTweetApp app = (MyTweetApp.getApp());
    public ArrayList<Tweet> allTweets;
    public DbHelper dbHelper;

    /**
     *  tweet_list constructor used to implement the DbHelper class
     *  to assign the database content to the local array contents
     *  try and catch exception handler
     *
     *  @param context   the current context
     */
    public Tweet_List(Context context) {
        try {
            dbHelper = new DbHelper(context);
            allTweets = (ArrayList<Tweet>) dbHelper.selectTweets();

        } catch (Exception e) {
            info(this, "Error loading tweets: " + e.getMessage());
            allTweets = new ArrayList<>();
        }
    }


    /**
     * Add incoming tweet to both local and database storage
     *
     * @param tweet The tweet object to be added to local and database storage.
     */
    public void addTweet(Tweet tweet) {

        dbHelper.addTweet(tweet);
        allTweets.add(tweet);

    }


    /**
     * Obtain specified tweet from local list and return.
     *
     * @param id The Long id identifier of the tweet sought.
     * @return The specified tweet if it exists.
     */

    public Tweet getTweet(String id) {
        Log.i(this.getClass().getSimpleName(), "String id: " + id);

        for (Tweet t : allTweets) {
            if (t._id.equals(id)) {
                return t;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }

    /**
     * Empties the contents/tweets from the local array and
     * from the database.
     */
    public void dumpContents() {
        allTweets.clear();
        dbHelper.deleteTweets();

       Call<Tweet> call1 = (Call) app.tweetService.deleteUserTweets(app.currentUser._id);
        call1.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse (Call<Tweet> call, Response<Tweet> response){
                Log.v("users", " message delete all tweet response");
                //allTweets.clear();
                //dbHelper.deleteTweets();
                app.tweetServiceAvailable = true;


            }
            @Override
            public void onFailure (Call <Tweet> call, Throwable t){
                Log.v("users", " message delete all tweet failure");
                app.tweetServiceAvailable = false;
            }
        });


    }

    /**
     * Delete Tweet object from local and remote storage
     *
     * @param tweet Tweet object for deletion.
     */
    public void deleteTweet(Tweet tweet) {

       //final Tweet t = tweet;
        dbHelper.deleteTweet(tweet);
        allTweets.remove(tweet);

        Call<Tweet> call1 = (Call) app.tweetService.deleteTweet(tweet._id);
        call1.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse (Call<Tweet> call, Response<Tweet> response){
                Log.v("users", " message delete tweet response");

                //dbHelper.deleteTweet(t);
                //allTweets.remove(t);
                app.tweetServiceAvailable = true;

            }
            @Override
            public void onFailure (Call <Tweet> call, Throwable t){
                Log.v("users", " message delete tweet failure unable to delete now");
                app.tweetServiceAvailable = false;
            }
        });


    }

}