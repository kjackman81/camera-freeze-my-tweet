package org.wit.mytweet.models;

import java.util.Random;

public class User {

    /**
     * variables used during the implementation of class
     */
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public String _id;

    /**
     * constructor used for instantiating, with parameters
     * and assign id
     *
     * @param firstName the string  first name of the user
     * @param lastName  the string last name of the user
     * @param email     the string email of the user
     * @param password  the string password of the user
     */

    public User(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;

        //id = unsignedLong();
    }

    /**
     * default constructor takes no parameters
     */
    public User() {

    }

    /**
     * method used to return a RANDOM id which is positive
     *
     * @return long the positive id long number
     */
    private Long unsignedLong() {
        long rndVal;
        do {
            rndVal = new Random().nextLong();
        } while (rndVal <= 0);
        return rndVal;
    }
}