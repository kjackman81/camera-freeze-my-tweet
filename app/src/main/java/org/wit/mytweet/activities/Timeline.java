package org.wit.mytweet.activities;

import org.wit.mytweet.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;


public class Timeline extends AppCompatActivity {


    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * container used to implement fragments
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {
            fragment = new TimelineFragment();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
        }
    }
    @Override
    public void onPause() {

        super.onPause();

    }
}