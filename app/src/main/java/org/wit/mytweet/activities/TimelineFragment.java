package org.wit.mytweet.activities;

import static org.wit.mytweet.helpers.IntentHelper.startActivityWithData;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweet_List;
import org.wit.mytweet.settings.SettingsActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimelineFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener {

    /**
     * variables used during the implementation of class
     */

    public Tweet_List tweet_list;
    public TimelineAdapter adapter;
    public MyTweetApp app;
    private ListView listView;
    //private Handler mHandler;
    private Timer timer2;
    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour
     * assigns variables and adapter
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);
        app = MyTweetApp.getApp();
        tweet_list = app.tweet_list;
        //Collections.reverse(tweet_list.allTweets);
        adapter = new TimelineAdapter(getActivity(),tweet_list.allTweets);
        setListAdapter(adapter);
       // timer2 = new Timer();

        //this.mHandler = new Handler();

        //this.mHandler.postDelayed(m_Runnable,50000);

        //autoRefreshTweets();

    }

  /*  private final Runnable m_Runnable = new Runnable()
    {
        public void run()

        {
            refresh();
        }

    };
*/
    /**
     * method called to have the list fragment instantiate its user interface view,
     * calls the super method first and adds Listener,
     *
     * @param savedInstanceState if fragment is being re-constructed
     *                           from a previous saved state as given here
     * @param inflater           object used to inflate any views,
     * @param parent             this is the parent view that the fragment's UI should be attached to
     * @return v return with the fragment list view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);




        return v;
    }

    /**
     * after the activity is resumed it calls the super method first,
     * notifies the adapter of any changes to the data set/array
     */
    @Override
    public void onResume() {

        super.onResume();
        ((TimelineAdapter) getListAdapter()).notifyDataSetChanged();


    }

    /**
     * first calls super method,
     * inflates the contents of the Activity's menu_timeline_activity options menu,
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_timeline_activity, menu);

    }




    /**
     * method used to call tweet_list method to empty the contents of the array,
     * restarts the timeline activity with timeline class call
     */
    public void clearTweetsButtonPressed() throws IOException {

        tweet_list.dumpContents();
        startActivity(new Intent(getActivity(), Timeline.class));

    }

    /**
     * method used to start the welcome activity calls the welcome class
     */
    public void welcomeButtonPressed() {
        startActivity(new Intent(getActivity(), Welcome.class));

    }

    /**
     * method used to differentiate which option in the menu has been selected from there id
     * and call the corresponding method
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings_lable:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;

            case R.id.menu_clear_label:
                try {
                    clearTweetsButtonPressed();
                } catch (IOException e) {
                    e.printStackTrace();

                }
                break;

            case R.id.menu_welcome_label:
                welcomeButtonPressed();
                break;

            case R.id.new_tweet:
                startActivity(new Intent(getActivity(), MyTweet.class));
                break;

            case R.id.action_refresh:
               refresh();
                break;

            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    /**
     * when an item in the listView is clicked it starts up the pager,
     * populates it with the selected fragment and its accompanying data
     * such as position and id
     *
     * @param l        lhe ListView where the click happened-on timeline
     * @param v        the view with the listView that was clicked - item on timeline
     * @param position the position of the view in the list
     * @param id       the row id of the item that was clicked
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tweet = ((TimelineAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), MyTweetPager.class);
        i.putExtra(MyTweetFragment.EXTRA_TWEET_ID, tweet._id);
        startActivityForResult(i, 0);
    }

    /**
     * callback method to be invoked when an item in this AdapterView has been clicked
     *
     * @param parent   adapterView where the click happened
     * @param view     the view within the adapterView that was clicked
     * @param position the position of the view in the adapter
     * @param id       the row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Tweet tweet = adapter.getItem(position);
        startActivityWithData(getActivity(), MyTweetPager.class, "TWEET_ID", tweet._id);
    }



/*============================================================================================================================*/

    /**
     * method used to inflate/create the actionMode menu capability
     *
     * @param actionMode the actionMode being created
     * @param menu       the menu used to populate action buttons
     */
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.menu_tweet_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    /**
     * this method is used to report a user click on the action button from its id,
     * then calls the deleteTweet method to then carry out the remove of selected data
     *
     * @param actionMode the current ActionMode
     * @param menuItem   the menu item that was clicked
     */
    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_item_delete_tweet:
                deleteTweet(actionMode);
                return true;
            default:
                return false;
        }

    }

    /**
     * method used to delete selected tweets from the listView,
     * checks which tweets have been selected isChecked,
     * adapter gets the item, then calls the delete method to carry out remove,
     * finishes and updates adapter of said change
     *
     * @param actionMode method of retrieving data for deletion
     */
    private void deleteTweet(ActionMode actionMode) {
        for (int i = adapter.getCount() - 1; i >= 0; i--) {
            if (listView.isItemChecked(i)) {
                tweet_list.deleteTweet(adapter.getItem(i));
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();

        startActivity(new Intent(getActivity(), Timeline.class));

    }

 public void autoRefreshTweets(){
        final Handler handler = new Handler();
        TimerTask testing = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        refresh();
                    }

                });
            }
        };
        timer2.schedule(testing, 50000);
    }

    @Override
    public void onPause() {
        super.onPause();
        //timer2.cancel();
    }


    public void refresh() {
if(app.tweetServiceAvailable == true) {
    Call<List<Tweet>> call3 = (Call) app.tweetService.getAllOurTweets(app.currentUser._id);
    call3.enqueue(new Callback<List<Tweet>>() {

        @Override
        public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {

            ArrayList<Tweet> tweets = new ArrayList<>(response.body());

            tweet_list.allTweets.clear();
            app.dbHelper.deleteTweets();

            for (Tweet t : tweets) {
                tweet_list.addTweet(t);
            }

            startActivity(new Intent(getActivity(), Timeline.class));

        }

        @Override
        public void onFailure(Call<List<Tweet>> call, Throwable t) {
            Log.v("users", "unable to refresh tweets");
            app.tweetServiceAvailable = false;

        }
    });
}else{
     Toast.makeText(getActivity(), "unable to refresh at this time", Toast.LENGTH_SHORT).show();
}


    }


    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
    }


}

/**
 * class that extends ArrayAdapter to facilitate the insertion of the allTweets array,
 * this puts the contents of the array into individual activity_item_timeline
 */
class TimelineAdapter extends ArrayAdapter<Tweet> {
    private Context context;

    //List<Tweet> allTweets;
    /**
     * constructor used to instantiate  adapter and assign the current context,
     * super call to parent first
     *
     * @param context   the current context
     * @param allTweets the array to be represented in the ListView
     */
    public TimelineAdapter(Context context, ArrayList<Tweet> allTweets) {
        super(context, 0, allTweets);
        this.context = context;
        //this.allTweets = allTweets;
        //Collections.reverse(this.allTweets);

    }


    /**
     * method to return the displayed view of a specific position in the array
     * assign retrieved id parameters to current variables
     *
     * @param position    the position of the item within the array  hose view we want
     * @param convertView the old view to reuse, if possible
     *                    check that this view is non-null
     * @param parent      the parent that this view will eventually be attached to
     * @return view corresponding to the data at the specified position.
     */
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_item_timeline, null);


        }
        Tweet tweet = getItem(position);

        TextView tweetMessage = (TextView) convertView.findViewById(R.id.item_timeline_message);
        tweetMessage.setText(tweet.message);

        TextView tweetDate = (TextView) convertView.findViewById(R.id.item_timeline_date);
        tweetDate.setText(tweet.createdAt);

        TextView tweetBlogger = (TextView) convertView.findViewById(R.id.item_timeline_blogger);
        tweetBlogger.setText(tweet.blogger.firstName + ' ' + tweet.blogger.lastName);


        return convertView;
    }


}
