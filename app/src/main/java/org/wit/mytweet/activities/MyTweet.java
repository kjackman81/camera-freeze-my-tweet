
package org.wit.mytweet.activities;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import org.wit.mytweet.R;


public class MyTweet extends AppCompatActivity {
    /**
     * toolbar within the activity that may display items
     */
    ActionBar actionBar;


    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * assign actionBar,
     * container used to implement fragments
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        actionBar = getSupportActionBar();

        FragmentManager manager = getFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {
            fragment = new MyTweetFrag();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
        }
    }
    @Override
    public void onPause() {

        super.onPause();

    }

}