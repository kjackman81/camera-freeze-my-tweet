package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.app.tweet.RetrofitServiceFactory;
import org.wit.mytweet.app.tweet.TweetService;
import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;
import java.util.PriorityQueue;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity implements View.OnClickListener, Callback<Token> {

    /**
     * variables used during the implementation of class
     */
    private EditText loginEmail;
    private EditText loginPassword;
    private Button loginButton;
    MyTweetApp app;
    User foundUser;


    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour
     * assigns variables and listeners
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginEmail = (EditText) findViewById(R.id.loginEmail);
        loginPassword = (EditText) findViewById(R.id.loginPassword);
        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        foundUser = new User();
        app = (MyTweetApp) getApplication();
    }

    /**
     * method called when the implemented listener is invoked,
     * make use of switch statement to differentiate
     *
     * @param v the view in which the listener was triggered
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case (R.id.loginButton):
                loginButtonPressed();
                break;
        }
    }

    /**
     * method called when the login button is pressed,
     * checks to make sure there are no empty fields,
     * uses the dbHelper method to retrieve the user matching the email if any,
     * authenticates there password, then allows or denies entry
     */

    public void loginButtonPressed() {


        String loginEmailString = loginEmail.getText().toString();
        String loginPasswordString = loginPassword.getText().toString();
        if (loginEmailString.isEmpty() || loginPasswordString.isEmpty()) {
            Toast.makeText(this, "Please fill out all FIELDS!!!", Toast.LENGTH_SHORT).show();
        } else {

            User u = new User();
            u.email = loginEmailString;
            u.password = loginPasswordString;

            Call<Token> call = (Call) app.tweetServiceOpen.authenticate(u);
            call.enqueue(this);
        }

    }

    @Override
    public void onResponse(Call<Token> call, Response<Token> response) {
        Token auth = response.body();
        if(auth.user != null) {
            app.currentUser = auth.user;
            app.dbHelper.deleteUsers();
            app.dbHelper.addUser(auth.user);
            app.tweetService = RetrofitServiceFactory.createService(TweetService.class, auth.token);
            Log.v("users", "Authenticated " + app.currentUser.firstName + ' ' + app.currentUser.lastName);

            app.tweetServiceAvailable = true;

            app.refreshTweets();


            Toast.makeText(this, "You Have Arrived!: ", Toast.LENGTH_LONG).show();

            startActivity(new Intent(this, Timeline.class));
        }


        else
        {
            String loginEmailString = loginEmail.getText().toString();
            String loginPasswordString = loginPassword.getText().toString();
            User foundUser = app.dbHelper.selectUser(loginEmailString);

            if (foundUser.password != null && foundUser.password.equals(loginPasswordString)) {
                app.currentUser = foundUser;
                Toast.makeText(this, "You Have Arrived in without cloud help: ", Toast.LENGTH_LONG).show();
                app.tweetServiceAvailable = false;
                startActivity(new Intent(this, Timeline.class));
            } else {

                loginEmail.setText("");
                loginPassword.setText("");
                Toast.makeText(this, "unable to login at this time", Toast.LENGTH_SHORT).show();

            }
        }
    }


    @Override
    public void onFailure(Call<Token> call, Throwable t) {
        String loginEmailString = loginEmail.getText().toString();
        String loginPasswordString = loginPassword.getText().toString();

        if (loginEmailString.isEmpty() || loginPasswordString.isEmpty()) {
            Toast toast = Toast.makeText(this, "Please fill out all FIELDS!!!", Toast.LENGTH_SHORT);
            toast.show();
        } else {

            User foundUser = app.dbHelper.selectUser(loginEmailString);

            if (foundUser.password != null && foundUser.password.equals(loginPasswordString)) {
                app.currentUser = foundUser;
                Toast.makeText(this, "You Have Arrived! in with cloud failure: ", Toast.LENGTH_LONG).show();

                app.tweetServiceAvailable = false;

                startActivity(new Intent(this, Timeline.class));

            } else {

                loginEmail.setText("");
                loginPassword.setText("");
                Toast.makeText(this, "unable to login at this time", Toast.LENGTH_SHORT).show();

            }
        }

    }
    @Override
    public void onPause() {

        super.onPause();

    }

}




