package org.wit.mytweet.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.helpers.ContactHelper;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweet_List;
import static org.wit.mytweet.helpers.CameraHelper.showPhoto;
import android.widget.ImageView;

import static org.wit.mytweet.helpers.IntentHelper.navigateUp;

import java.util.Date;
import android.content.Intent;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wit.mytweet.helpers.ContactHelper.sendEmail;

public class MyTweetFrag extends android.app.Fragment implements View.OnClickListener, TextWatcher,View.OnLongClickListener, Callback<Tweet> {

    /**
     * variables used during the implementation of class
     */
    private TextView charAmount;
    private EditText tweetMessage;
    private Button tweetSelectContactButton;
    private Long date;
    private Tweet_List tweet_list;
    private static final int REQUEST_CONTACT = 1;
    private String emailAddress = "";
    private MyTweetApp app;
    public Tweet returnedTweet;
    private Intent data;
    private static final int REQUEST_PHOTO = 0;
    private ImageView cameraButton;
    private ImageView photoView;
    private Tweet tweet;
    public static final String EXTRA_TWEET_ID = "myTweet.TWEET_ID";
    public static final String EXTRA_PHOTO_DATA = "org.wit.mytweet.photo.data";

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * initializes variables
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        app = MyTweetApp.getApp();
        tweet_list = app.tweet_list;
        tweet = new Tweet();

    }

    @Override
    public void onStart()
    {
        super.onStart();
        //display thumbnail photo
       // showPhoto(getActivity(), tweet, photoView);
    }

    /**
     * method called to have the fragment instantiate its user interface view in the container,
     * calls the addListeners method and assigns actionbar,
     *
     * @param savedInstanceState if fragment is being re-constructed
     *                           from a previous saved state as given here
     * @param inflater           object used to inflate any views in the MyTweetFrag fragment,
     * @param parent             this is the parent view that the fragment's UI should be attached to
     * @return v return with the fragment_mytweet view
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_mytweet, parent, false);


        MyTweet mytweet = (MyTweet) getActivity();
        mytweet.actionBar.setDisplayHomeAsUpEnabled(true);

        addListeners(v);

        return v;
    }

    /**
     * method used to assign listeners to elements on view
     *
     * @param v the view the button are on
     */

    private void addListeners(View v) {
        Button tweetButton = (Button) v.findViewById(R.id.tweetButton);
        tweetButton.setOnClickListener(this);

        tweetSelectContactButton = (Button) v.findViewById(R.id.tweetSelectContactButton);
        tweetSelectContactButton.setOnClickListener(this);

        Button tweetEmailViaButton = (Button) v.findViewById(R.id.tweetEmailViaButton);
        tweetEmailViaButton.setOnClickListener(this);


        charAmount = (TextView) v.findViewById(R.id.charAmount);

        tweetMessage = (EditText) v.findViewById(R.id.tweetMessage);
        tweetMessage.addTextChangedListener(this);


        TextView tweetDate = (TextView) v.findViewById(R.id.tweetDate);
        date = new Date().getTime();
        tweetDate.setText(dateString());

        cameraButton = (ImageView) v.findViewById(R.id.camera_button);
        photoView = (ImageView) v.findViewById(R.id.mytweet_imageView);
        cameraButton.setOnClickListener(this);
        //photoView.setOnLongClickListener(this);

    }

    /**
     * method called when the activity is going into the background,
     * but has not been killed, calls super method first
     */
    @Override
    public void onPause() {

        super.onPause();

    }

    /**
     * method called whenever an item in your options menu is selected,
     * in this case the navigateUp method used to go back to the timeline/home
     *
     * @param item menu item that was selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method called when the tweet button is pressed,
     * creates a new tweet, assigns the parameters from the view elements
     * if tweet message is empty it disregards
     */
    public void messageTweetButtonPressed() {
       // Tweet tweet = new Tweet();
        tweet.message = tweetMessage.getText().toString();
        Log.v("tweetMessage", tweet.message);

        Log.v("users", " message tweet button press 0");
        Log.v("users", app.currentUser.firstName);

        if (!tweet.message.isEmpty()) {

            Toast.makeText(getActivity(), "Sending Message", Toast.LENGTH_SHORT).show();

            Log.v("users", " message tweet button press 1");
            Call<Tweet> call1 = (Call) app.tweetService.createTweet(app.currentUser._id, tweet);
            call1.enqueue(this);

        } else {
            Toast.makeText(getActivity(), "Empty message not added to Timeline", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), Timeline.class));
        }


    }

    /**
     * method called when the email tweet button is pressed,
     * assigns the subject string,
     * invokes the ContactHelper method sendMail to facilitate sending mail for contacts
     */
    public void tweetEmailButtonPressed() {
        String subject = "From KJackMan Tweeter";
        sendEmail(getActivity(), emailAddress, subject, tweetMessage.getText().toString());


    }

    /**
     * method used to format date,
     *
     * @return String the date in a string format
     */
    private String dateString() {
        String dateFormat = "EEE d MMM yyyy H:mm";
        return android.text.format.DateFormat.format(dateFormat, date).toString();
    }

    /**
     * method  used to facilitate multiple listeners on the view,
     * also facilitates the selection from contacts for the mail,
     *
     * @param v the view in which the listeners are on
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.tweetSelectContactButton):
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;


            case (R.id.tweetEmailViaButton):
                tweetEmailButtonPressed();
                tweetSelectContactButton.setText("SELECT CONTACT");
                break;


            case (R.id.tweetButton):
                messageTweetButtonPressed();
                break;

            case R.id.camera_button:
                Intent ic = new Intent(getActivity(), MyTweetCameraActivity.class);
                startActivityForResult(ic, REQUEST_PHOTO);
                break;

        }
    }

    /**
     * implemented  methods from the TextWatcher interface
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    /**
     * method used to notify after any text change to a listener on the view,
     * uses the length of the s variable to assign charAmount element
     *
     * @param s the string being passed each time after text change
     */
    @Override
    public void afterTextChanged(Editable s) {
        int totalCharInt = 140;
        int messageLength = s.length();

        int charsLeft = (totalCharInt - messageLength);

        String charsLeftString = Integer.toString(charsLeft);

        charAmount.setText(charsLeftString);

    }

    /**
     * method called when a launch activity exits,
     * used here to to return the email of the selected contact/recipient,
     * also assigns to button,
     *
     * @param requestCode integer request code originally supplied to startActivityForResult()
     *                    implemented in the onclick tweetSelectContactButton
     * @param resultCode  integer result code returned by the child activity through its setResult()
     * @param data        an Intent, which can return result data to the caller
     */
    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                String name = ContactHelper.getContact(getActivity(), data);
                emailAddress = ContactHelper.getEmail(getActivity(), data);
                String recipient = name + " : " + emailAddress;
                tweetSelectContactButton.setText(recipient);
                break;
        }
    }*/ @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;

            case REQUEST_PHOTO:
                String filename = data.getStringExtra(MyTweetCameraActivity.EXTRA_PHOTO_FILENAME);
               // String dataP = data.getStringExtra(MyTweetCameraActivity.EXTRA_PHOTO_DATA);
                //byte [] b = StringToBitMap(dataP);

                  // String arryb= b.toString();
               // Log.v("users", b + "    2");
                if (!filename.isEmpty())
                {

                    //tweet.photo = arryb;
                    showPhoto(getActivity(), filename, photoView );
                }
                break;
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;
        }
    }*/

    public byte [] StringToBitMap(String encodedString) {

            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);


            return encodeByte;

    }

    private void readContact() {
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        String recipient = name + " : " + emailAddress;
        tweetSelectContactButton.setText(recipient);

    }

    /**
     * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
     * This is an override of FragmentCompat.onRequestPermissionsResult
     *
     * @param requestCode Example REQUEST_CONTACT
     * @param permissions String array of permissions requested.
     * @param grantResults int array of results for permissions request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    readContact();
                }
                break;
            }
        }
    }

    /**
     * Bespoke method to check if read contacts permission exists.
     * If it exists then the contact sought is read.
     * Otherwise, the method FragmentCompat.request permissions is invoked and
     * The response is via the callback onRequestPermissionsResult.
     * In onRequestPermissionsResult, on successfully being granted permission then the sought contact is read.
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            readContact();
        }
        else {
            // Invoke callback to request user-granted permission
            FragmentCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CONTACT);
        }
    }






    @Override
    public void onResponse(Call<Tweet> call, Response<Tweet> response) {
        Log.v("users", " message tweet response");
        returnedTweet = response.body();
        if (returnedTweet.blogger != null) {
            tweet_list.addTweet(returnedTweet);
            Log.v("users",returnedTweet._id);
            app.tweetServiceAvailable = true;
            app.refreshTweets();
            Toast.makeText(getActivity(), "Timeline Updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), Timeline.class));
        }else{
            Toast.makeText(getActivity(), "Timeline error!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), Timeline.class));
        }



    }

    @Override
    public void onFailure(Call<Tweet> call, Throwable t) {

        Log.v("users", " message tweet failure");
        Toast.makeText(getActivity(), "Unable to send tweet at this time!", Toast.LENGTH_LONG).show();
        app.tweetServiceAvailable = false;
        startActivity(new Intent(getActivity(), Timeline.class));
    }


     //====================== longpress thumbnail ===================================

   //Long press the bitmap image to view photo in single-photo gallery

    @Override
    public boolean onLongClick(View v)
    {
        Intent i = new Intent(getActivity(), MyTweetGalleryActivity.class);
        i.putExtra(EXTRA_TWEET_ID, tweet._id);
        startActivity(i);
        return true;
    }
}