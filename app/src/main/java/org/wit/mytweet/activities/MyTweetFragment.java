package org.wit.mytweet.activities;

import android.app.Activity;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.Manifest;
import android.content.pm.PackageManager;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.helpers.ContactHelper;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweet_List;

import static org.wit.mytweet.helpers.IntentHelper.navigateUp;

import android.content.Intent;

import static org.wit.mytweet.helpers.ContactHelper.sendEmail;
import static org.wit.mytweet.helpers.CameraHelper.showPhoto;
import android.widget.ImageView;

public class MyTweetFragment extends android.app.Fragment implements View.OnClickListener, View.OnLongClickListener{

    /**
     * variables used during the implementation of class
     */

    private Button tweetButton;
    private TextView charAmount;
    private EditText tweetMessage;
    private TextView tweetDate;
    private Button tweetSelectContactButton;
    private Tweet_List tweet_list;
    private Tweet tweet;
    private static final int REQUEST_CONTACT = 1;
    public static final String EXTRA_TWEET_ID = "myTweet.TWEET_ID";
    private String emailAddress = "";
    private Intent data;
    private static final int REQUEST_PHOTO = 0;
    private ImageView cameraButton;
    private ImageView photoView;

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * initializes variables,
     * uses the  embedded tweet id to find the tweet in the tweet_list
     * and assigns it to the tweet
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        MyTweetApp app = MyTweetApp.getApp();
        tweet_list = app.tweet_list;


        String tweetID = (String) getArguments().getSerializable(EXTRA_TWEET_ID);
        tweet = tweet_list.getTweet(tweetID);
    }

    /**
     * method called to have the fragment instantiate its user interface view in the pager,
     * calls the updateControls and addListeners method,
     *
     * @param savedInstanceState if fragment is being re-constructed
     *                           from a previous saved state as given here
     * @param inflater           object used to inflate any views in the MyTweetFrag fragment,
     * @param parent             this is the parent view that the fragment's UI should be attached to
     * @return v return with the fragment_mytweet view
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_mytweet, parent, false);

        addListeners(v);

        if (tweet.message != null) {
            updateControls(tweet);
        }

        return v;
    }

    /**
     * method used to assign listeners to elements on view
     *
     * @param v the view the button are on
     */

    private void addListeners(View v) {
        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        tweetButton.setOnClickListener(this);

        tweetSelectContactButton = (Button) v.findViewById(R.id.tweetSelectContactButton);
        tweetSelectContactButton.setOnClickListener(this);

        Button tweetEmailViaButton = (Button) v.findViewById(R.id.tweetEmailViaButton);
        tweetEmailViaButton.setOnClickListener(this);


        charAmount = (TextView) v.findViewById(R.id.charAmount);

        tweetMessage = (EditText) v.findViewById(R.id.tweetMessage);


        tweetDate = (TextView) v.findViewById(R.id.tweetDate);

        cameraButton = (ImageView) v.findViewById(R.id.camera_button);
        photoView = (ImageView) v.findViewById(R.id.mytweet_imageView);

        cameraButton.setOnClickListener(this);
        cameraButton.setVisibility(View.GONE);

        photoView.setOnLongClickListener(this);


    }
    @Override
    public void onStart()
    {
        super.onStart();
        //display thumbnail photo
        //showPhoto(getActivity(), tweet, photoView);
    }

    /**
     * method called when the activity is going into the background,
     * but has not been killed, calls super method first
     */
    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * method called whenever an item in your options menu is selected,
     * in this case the navigateUp method used to go back to the timeline/home
     *
     * @param item menu item that was selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method called when the activity is selected to return
     *
     * @param tweet the tweet used to populate the pagers swipe functionality
     */

    public void updateControls(Tweet tweet) {
        tweetMessage.setText(tweet.message);
        tweetMessage.setEnabled(false);
        tweetButton.setClickable(false);
        tweetDate.setText(tweet.createdAt);
        int totalCharInt = 140;
        charAmount.setText(Integer.toString(totalCharInt - tweetMessage.length()));


    }

    /**
     * method called when the email tweet button is pressed,
     * assigns the subject string,
     * invokes the ContactHelper method sendMail to facilitate sending mail for contacts
     */
    public void tweetEmailButtonPressed() {
        String subject = "From KJackMan Tweeter";
        sendEmail(getActivity(), emailAddress, subject, tweetMessage.getText().toString());

    }

    /**
     * method  used to facilitate multiple listeners on the view,
     * also facilitates the selection from contacts for the mail,
     *
     * @param v the view in which the listeners are on
     */
    @Override
    public void onClick(View v) {
        String selectedContact= "SELECT CONTACT";
        switch (v.getId()) {
            case (R.id.tweetSelectContactButton):
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;


            case (R.id.tweetEmailViaButton):
                tweetEmailButtonPressed();
                tweetSelectContactButton.setText(selectedContact);
                break;

            case R.id.camera_button:
                Intent ic = new Intent(getActivity(), MyTweetCameraActivity.class);
                startActivityForResult(ic, REQUEST_PHOTO);
                break;

        }
    }

    /**
     * method called when a launch activity exits,
     * used here to to return the email of the selected contact/recipient,
     * also assigns to button,
     *
     * @param requestCode integer request code originally supplied to startActivityForResult()
     *                    implemented in the onclick tweetSelectContactButton
     * @param resultCode  integer result code returned by the child activity through its setResult()
     * @param data        an Intent, which can return result data to the caller
     */
   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                String name = ContactHelper.getContact(getActivity(), data);
                emailAddress = ContactHelper.getEmail(getActivity(), data);
                String recipient = name + " : " + emailAddress;
                tweetSelectContactButton.setText(recipient);
                break;
        }
    }
*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;

            case REQUEST_PHOTO:
                String filename = data.getStringExtra(MyTweetCameraActivity.EXTRA_PHOTO_FILENAME);
                if (!filename.isEmpty())
                {
                    tweet.photo = filename;
                    showPhoto(getActivity(), filename, photoView );
                }
                break;
        }
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;
        }
    }*/

    private void readContact() {
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        String recipient = name + " : " + emailAddress;
        tweetSelectContactButton.setText(recipient);

    }

    /**
     * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
     * This is an override of FragmentCompat.onRequestPermissionsResult
     *
     * @param requestCode Example REQUEST_CONTACT
     * @param permissions String array of permissions requested.
     * @param grantResults int array of results for permissions request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    readContact();
                }
                break;
            }
        }
    }

    /**
     * Bespoke method to check if read contacts permission exists.
     * If it exists then the contact sought is read.
     * Otherwise, the method FragmentCompat.request permissions is invoked and
     * The response is via the callback onRequestPermissionsResult.
     * In onRequestPermissionsResult, on successfully being granted permission then the sought contact is read.
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            readContact();
        }
        else {
            // Invoke callback to request user-granted permission
            FragmentCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CONTACT);
        }
    }

    @Override
    public boolean onLongClick(View v)
    {
        Intent i = new Intent(getActivity(), MyTweetGalleryActivity.class);
        i.putExtra(EXTRA_TWEET_ID, tweet._id);
        startActivity(i);
        return true;
    }

}