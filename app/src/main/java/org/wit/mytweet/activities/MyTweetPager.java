package org.wit.mytweet.activities;


import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweet_List;


import java.util.ArrayList;


public class MyTweetPager extends AppCompatActivity {

    /**
     * variables used during the implementation of class
     */
    private ViewPager viewPager;
    private ArrayList<Tweet> allTweets;
    private PagerAdapter pagerAdapter;


    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * instantiates viewPager and passes in the allTweets array
     * through the pagerAdaptor,
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        pagerAdapter = new PagerAdapter(getFragmentManager(), allTweets);
        pagerAdapter.notifyDataSetChanged();
        viewPager.setAdapter(pagerAdapter);
        setCurrentItem();
    }

    @Override
    public void onResume(){
        super.onResume();
        //viewPager.notifyAll();
    }


    /**
     * iterates through the allTweets array of MyTweetFragment fragments id's
     * to populate the viwPager
     */
    private void setCurrentItem() {
        String tweetId = (String)getIntent().getSerializableExtra(MyTweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < allTweets.size(); i++) {
            if ( allTweets.get(i)._id.equals(tweetId)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    /**
     * method used to assign the elements of the  allTweets array
     * from the creation of the app where
     * the sql database is implemented into the tweet_list
     */
    private void setTweetList() {
        MyTweetApp app = (MyTweetApp) getApplication();
        Tweet_List tweet_list = app.tweet_list;
        allTweets = tweet_list.allTweets;
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Tweet> allTweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> allTweets) {
            super(fm);
            this.allTweets = allTweets;
        }

        /**
         * method used to return the count or size of the allTweets array
         *
         * @return int the size of the array
         */
        @Override
        public int getCount() {
            return allTweets.size();
        }

        /**
         * method used to get each individual MyTweetFragment
         * from the array to populate the position
         *
         * @return fragment the fragment to be viewed
         */
        @Override
        public Fragment getItem(int pos) {
            Tweet tweet = allTweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(MyTweetFragment.EXTRA_TWEET_ID, tweet._id);
            MyTweetFragment fragment = new MyTweetFragment();
            fragment.setArguments(args);
            return fragment;
        }

    } @Override
    public void onPause() {

        super.onPause();

    }

}
