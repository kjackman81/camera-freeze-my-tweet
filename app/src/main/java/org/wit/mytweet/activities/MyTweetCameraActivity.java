package org.wit.mytweet.activities;

import org.wit.mytweet.R;
import android.graphics.Bitmap;

import static org.wit.mytweet.helpers.FileIOHelper.byteArray;
import static org.wit.mytweet.helpers.FileIOHelper.writeBitmap;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import java.util.UUID;

public class MyTweetCameraActivity  extends AppCompatActivity implements View.OnClickListener {

    private Bitmap tweetPhoto;
    private Button savePhoto;
    private Button takePhoto;
    private ImageView tweetImage;
    private static  final int CAMERA_RESULT = 5;
    public static final String EXTRA_PHOTO_FILENAME = "org.wit.mytweet.photo.filename";
    public static final String EXTRA_PHOTO_DATA = "org.wit.mytweet.photo.data";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweet_photo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tweetImage = (ImageView) findViewById(R.id.tweetImage);
        savePhoto = (Button) findViewById(R.id.savePhoto);
        takePhoto = (Button) findViewById(R.id.takePhoto);
        savePhoto.setEnabled(false);
        savePhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.takePhoto     : onTakePhotoClicked(v);
                break;

            case R.id.savePhoto     : onPictureTaken(tweetPhoto);
                break;
        }

    }

    public void onTakePhotoClicked(View v)
    {
        Log.v("users", " take photo button hit ");
        // Check for presence of device camera. If not present advise user and quit method.
        PackageManager pm = getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "Camera app not present on this device", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.v("users", " take photo button hit ");
        // The device has a camera app ... so use it.
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent,CAMERA_RESULT);
        savePhoto.setEnabled(true);
    }
    private void onPictureTaken(Bitmap data) {
        Log.v("users", " picture taken button hit ");
        String filename = UUID.randomUUID().toString() + ".png";

        Log.v("users", filename);
        if (writeBitmap(this, filename, data) == true) {
            Intent intent = new Intent();
            String data2 = byteArray(data).toString();

            Log.v("users", data.toString());
            Log.v("users", data2);
            intent.putExtra(EXTRA_PHOTO_DATA, data2);
            intent.putExtra(EXTRA_PHOTO_FILENAME, filename);
            setResult(Activity.RESULT_OK, intent);
        }
        else {
            setResult(Activity.RESULT_CANCELED);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case MyTweetCameraActivity.CAMERA_RESULT :
                if(data != null) {
                    processImage(data);
                }
                else {
                    Toast.makeText(this, "Camera failure: check simulated camera present emulator advanced settings",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void processImage(Intent data)
    {
        tweetPhoto = (Bitmap) data.getExtras().get("data");
        if(tweetPhoto == null)
        {
            Toast.makeText(this, "Attempt to take photo did not succeed", Toast.LENGTH_SHORT).show();
        }
        tweetImage.setImageBitmap(tweetPhoto);
    }

    @Override
    public void onPause() {

        super.onPause();

    }
}