package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Welcome extends AppCompatActivity implements View.OnClickListener {

    private MyTweetApp app;

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * assigns variables and listeners
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Button loginButtonWelcome = (Button) findViewById(R.id.loginButtonWelcome);
        loginButtonWelcome.setOnClickListener(this);
        Button signUpButtonWelcome = (Button) findViewById(R.id.signUpButtonWelcome);
        signUpButtonWelcome.setOnClickListener(this);
        app = (MyTweetApp) getApplication();

    }

    /**
     * method used to start or get the login activity
     *
     * @param v the view that invoked the listener on the button
     */
    public void welcomeLoginButtonPressed(View v) {

        startActivity(new Intent(this, Login.class));

       /* if (app.tweetServiceAvailable) {

            serviceAvailableMessage();
            startActivity(new Intent(this, Login.class));
        }
        else
        {
            serviceUnavailableMessage();
        }*/
    }

    /**
     * method used to start or get the sign up activity
     *
     * @param v the view that invoked the listener on the button
     */
    public void welcomeSignUpButtonPressed(View v) {

        startActivity(new Intent(this, SignUp.class));

       /* if (app.tweetServiceAvailable) {

            serviceAvailableMessage();
            startActivity(new Intent(this, SignUp.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
*/
    }

    /**
     * method called when the implemented listener is invoked,
     * make use of switch statement to differentiate
     *
     * @param v the view in which the listener was triggered
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.loginButtonWelcome:
                welcomeLoginButtonPressed(v);
                break;

            case R.id.signUpButtonWelcome:
                welcomeSignUpButtonPressed(v);
                break;
        }


    }


    @Override
    public void onResume() {

        super.onResume();

      /* Call<List<User>> call1 = (Call) app.tweetService.getUsers();
        call1.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                Log.v("users", " Welcome response");
                serviceAvailableMessage();
                app.users = response.body();
                String totalamount = Integer.toString(app.users.size());

                Log.v("users", totalamount);
                app.tweetServiceAvailable = true;
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.v("users", " Welcome fail");
                app.tweetServiceAvailable = false;
                serviceUnavailableMessage();
            }
        });*/


    }





    @Override
    public void onPause() {

        super.onPause();

    }


   /* void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }*/
}
