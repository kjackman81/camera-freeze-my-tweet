package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUp extends AppCompatActivity implements View.OnClickListener {

    /**
     * variables used during the implementation of class
     */
    private EditText signupFirstName;
    private EditText signupLastName;
    private EditText signupEmail;
    private EditText signupPassword;
    private MyTweetApp app;
    public User returnedUser;

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * assigns variables and listener
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        signupFirstName = (EditText) findViewById(R.id.signupFirstName);
        signupLastName = (EditText) findViewById(R.id.signupLastName);
        signupEmail = (EditText) findViewById(R.id.signupEmail);
        signupPassword = (EditText) findViewById(R.id.signupPassword);
        app = (MyTweetApp) getApplication();
        Button signupButton = (Button) findViewById(R.id.signupButton);
        signupButton.setOnClickListener(this);

    }

    /**
     * method called when the sign up button is pressed,
     * checks to make sure all fields are fill in,
     * saves the user in the database with use of the dbHandler addUser method
     * after deleting previous user
     */
    public void signupButtonPressed() {
        String signupFirstNameString = signupFirstName.getText().toString();
        String signupLastNameString = signupLastName.getText().toString();
        String signupEmailString = signupEmail.getText().toString();
        String signupPasswordString = signupPassword.getText().toString();

        if (signupFirstNameString.isEmpty() ||
                signupLastNameString.isEmpty() ||
                signupEmailString.isEmpty() ||
                signupPasswordString.isEmpty()) {
            Toast toast = Toast.makeText(this, "Please fill out all FIELDS!!!", Toast.LENGTH_SHORT);
            toast.show();
        } else {


            User user = new User(signupFirstNameString, signupLastNameString, signupEmailString, signupPasswordString);


            Call<User> call1 = (Call) app.tweetServiceOpen.createUser(user);
            call1.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    Log.v("users", " signUp response");

                    returnedUser = new User();

                    returnedUser = response.body();


                    Log.v("users", "users password   " + returnedUser.password);

                    app.dbHelper.deleteUsers();
                    app.dbHelper.addUser(returnedUser);


                    Log.v("users", "users id   " + returnedUser._id);
                    app.currentUser = returnedUser;

                    Log.v("users", "users firstName   " + returnedUser.firstName);
                    serviceAvailableMessage();
                    app.tweetServiceAvailable = true;


                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.v("users", " sign up fail");
                    serviceUnavailableMessage();
                    app.tweetServiceAvailable = false;

                }
            });


            signupFirstName.setText("");
            signupLastName.setText("");
            signupEmail.setText("");
            signupPassword.setText("");
            startActivity(new Intent(this, Login.class));


        }
    }

    /**
     * method called when the implemented listener is invoked,
     * make use of switch statement to differentiate
     *
     * @param v the view in which the listener was triggered
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case (R.id.signupButton):
                signupButtonPressed();
                break;
        }

    }

    void serviceUnavailableMessage() {
        Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage() {
        Toast toast = Toast.makeText(this, "Tweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }
    @Override
    public void onPause() {

        super.onPause();

    }

}
