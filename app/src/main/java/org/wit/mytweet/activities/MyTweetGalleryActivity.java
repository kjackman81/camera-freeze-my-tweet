package org.wit.mytweet.activities;


import org.wit.mytweet.R;
        import org.wit.mytweet.app.MyTweetApp;
        import org.wit.mytweet.models.Tweet_List;
        import org.wit.mytweet.models.Tweet;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.MenuItem;
        import android.widget.ImageView;
        import static org.wit.mytweet.helpers.CameraHelper.showPhoto;

public class MyTweetGalleryActivity extends AppCompatActivity
{

    public static   final String  EXTRA_PHOTO_FILENAME = "org.wit.mytweet.photo.filename";
    private ImageView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweet_gallery);
        photoView = (ImageView) findViewById(R.id.tweetGalleryImage);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //showPicture();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home  : onBackPressed();
                return true;
            default                 : return super.onOptionsItemSelected(item);
        }
    }

   /* private void showPicture()
    {
        String tweetId = (String)getIntent().getSerializableExtra(MyTweetFragment.EXTRA_TWEET_ID);
        MyTweetApp app = (MyTweetApp) getApplication();
        Tweet_List tweet_list = app.tweet_list;
        Tweet tweet = tweet_list.getTweet(tweetId);
        showPhoto(this, tweet,  photoView);
    }*/

    @Override
    public void onPause() {

        super.onPause();

    }
}