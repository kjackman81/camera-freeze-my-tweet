package org.wit.mytweet.settings;



import android.os.Bundle;
import android.preference.PreferenceFragment;
import org.wit.mytweet.R;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import static org.wit.mytweet.helpers.IntentHelper.navigateUp;


public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SharedPreferences prefs;

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * inflates the given the XML resource ID
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        setHasOptionsMenu(true);
    }

    /**
     * method calls super first,
     * assign default shared preferences to activity
     * registers listener
     */

    @Override
    public void onStart() {
        super.onStart();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity());

        prefs.registerOnSharedPreferenceChangeListener(this);

    }

    /**
     * method that returns to parent activity when menu item is selected
     *
     * @param item the menu item selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * method called when a change is made to the shared preferences,
     * logs a message of said change
     *
     * @param sharedPreferences the SharedPreferences that received the change
     * @param key               the key of the preference that was changed, added, or removed.
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    /**
     * method call when activity is stopped
     * calls super first
     * unregisters the preference listener
     */
    @Override
    public void onStop() {
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }
}

