package org.wit.mytweet.settings;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class SettingsActivity extends AppCompatActivity {
    @Override

    /**
     * method called when the activity is first created,
     * uses the super call to its parent for inherited behaviour,
     * instantiates the settingsFragment
     *
     * @param savedInstanceState containing the activity's previously frozen state,
     *                           if there was one
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            SettingsFragment fragment = new SettingsFragment();
            getFragmentManager().beginTransaction()
                    .add(android.R.id.content, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }


}