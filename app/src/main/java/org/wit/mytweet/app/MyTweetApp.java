package org.wit.mytweet.app;

import android.app.Application;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import org.wit.mytweet.app.tweet.RetrofitServiceFactory;
import org.wit.mytweet.app.tweet.TweetService;
import org.wit.mytweet.app.tweet.TweetServiceOpen;
import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweet_List;
import org.wit.mytweet.models.User;
import org.wit.mytweet.sqlite.DbHelper;


public class MyTweetApp extends Application /*implements Callback<Token>*/ {

    /**
     * variables used during the implementation of class
     */
    protected static MyTweetApp app;
    static final String TAG = "MyTweetApp";
    public Tweet_List tweet_list;
    public DbHelper dbHelper = null;

    public TweetServiceOpen tweetServiceOpen;
    public TweetService tweetService;


    public boolean tweetServiceAvailable = true;
    //public String service_url = "https://microblog-kjackman81gmailcom.herokuapp.com";
    //public String service_url = "http://10.0.2.2:4001";


    public User currentUser;
    //public Token token;
    public List<Tweet> tweets = new ArrayList<>();
    public List<User> users = new ArrayList<>();


    /**
     * method called when the app is first created,
     * uses the super call to its parent for inherited behaviour,
     * assigns app variable,
     * instantiates both tweet_list and database
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("users", "MyTweet app launched");
        app = this;
        tweet_list = new Tweet_List(getApplicationContext());
        dbHelper = new DbHelper(getApplicationContext());
        Log.d(TAG, "MyTweet app launched");

        super.onCreate();
        tweetServiceOpen = RetrofitServiceFactory.createService(TweetServiceOpen.class);
        Log.v("Tweet", "Tweet App Started");


    }

    public void refreshTweets() {


        if(app.tweetServiceAvailable) {


            Call<List<Tweet>> call3 = (Call) tweetService.getAllOurTweets(currentUser._id);
            call3.enqueue(new Callback<List<Tweet>>() {

                @Override
                public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                    tweetServiceAvailable = true;
                    tweets = response.body();
                    tweet_list.allTweets.clear();
                    dbHelper.deleteTweets();

                if(tweets !=  null) {
                   // Collections.reverse(tweets);

                    for (Tweet t : tweets) {
                        tweet_list.addTweet(t);
                    }
                }
                }

                @Override
                public void onFailure(Call<List<Tweet>> call, Throwable t) {
                    Log.v("users", "unable to refresh tweets");
                    tweetServiceAvailable = false;

                }
            });
        }else
        {
            Toast.makeText(this, "unable to refresh at this time", Toast.LENGTH_SHORT);
        }

    }


    /**
     * method used to return current app
     *
     * @return app the current application
     */
    public static MyTweetApp getApp() {

        return app;
    }

}
