package org.wit.mytweet.app.tweet;

/**
 * Created by Arthur1 on 06/12/2016.
 */


import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TweetServiceOpen
{

    @GET("/api/users/{id}")
    Call<User> getUser(@Path("id") String id);

    @POST("/api/users")
    Call<User> createUser(@Body User User);

    @POST("/api/users/authenticate")
    Call<Token> authenticate(@Body User user);
}
