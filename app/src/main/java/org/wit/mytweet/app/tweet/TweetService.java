package org.wit.mytweet.app.tweet;

/**
 * Created by Arthur1 on 06/12/2016.
 */

import org.wit.mytweet.models.Tweet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface TweetService {

    @POST("/api/users/{id}/tweets")
    Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

    @GET("/api/users/{id}/tweets")
    Call<List<Tweet>> getUserTweets(@Path("id") String id);

    @GET("/api/users/{id}/following")
    Call<List<Tweet>> getAllOurTweets(@Path("id") String id);

    @DELETE("/api/users/{id}/tweets")
    Call<Tweet> deleteUserTweets(@Path("id") String id);

    @DELETE("/api/tweets/{id}")
    Call<Tweet> deleteTweet(@Path("id") String id);

    @GET("/api/tweets")
    Call<List<Tweet>> getTweets();


}