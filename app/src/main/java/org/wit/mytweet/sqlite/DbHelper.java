package org.wit.mytweet.sqlite;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.ArrayList;
import java.util.List;


public class DbHelper extends SQLiteOpenHelper {
    /**
     * variables used during the implementation of class
     */

    static final String TAG = "DbHelper";
    static final String DATABASE_NAME = "tweets.db";
    static final int DATABASE_VERSION = 1;
    static final String TABLE_TWEETS = "tableTweets";
    static final String TABLE_USERS = "tableUsers";

    static final String PRIMARY_KEY = "id";
    static final String TWEET_MESSAGE = "tweetMessage";
    static final String DATE = "date";
    static final String PHOTO= "photo";
    static final String BLOGGER = "blogger";



    static final String FIRSTNAME = "firstName";
    static final String LASTNAME = "lastName";
    static final String EMAIL = "email";
    static final String PASSWORD = "password";


    Context context;

    /**
     * constructor  that calls super method first,
     * create the database object
     * and assign the context
     *
     * @param context to use to open or create the database
     */
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * this method is called when the database is created for the first time,
     * the creation and assigning of tables
     *
     * @param db the database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable =
                "CREATE TABLE tableTweets " +
                        "(id text primary key, " +
                        "tweetMessage text," +
                        "date text," +
                        "photo text," + "blogger text)";


        String createTableUsers =
                "CREATE TABLE tableUsers " +
                        "(id text primary key, " +
                        "firstName text," + "lastName text," + "email text," +
                        "password text)";

        db.execSQL(createTable);
        db.execSQL(createTableUsers);
        Log.d(TAG, "DbHelper.onCreated: " + createTable);
    }


    /**
     * method used to add a user to the database, insert values
     *
     * @param user reference to user object to be added to database
     */
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, user._id);
        values.put(FIRSTNAME, user.firstName);
        values.put(LASTNAME, user.lastName);
        values.put(EMAIL, user.email);
        values.put(PASSWORD, user.password);


        // Insert record
        db.insert(TABLE_USERS, null, values);
        db.close();
    }

    /**
     * method used to select a user from the database using the email as a parameter to search
     *
     * @param email the email of the user to search for
     * @return user the found user if any
     */
    public User selectUser(String email) {
        User user;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            user = new User();

            cursor = db.rawQuery("SELECT * FROM tableUsers WHERE email = ?", new String[]{email + ""});

            if (cursor.getCount() > 0) {
                int columnIndex = 0;
                cursor.moveToFirst();
                user._id = cursor.getString(columnIndex++);
                user.firstName = cursor.getString(columnIndex++);
                user.lastName = cursor.getString(columnIndex++);
                user.email = cursor.getString(columnIndex++);
                user.password = cursor.getString(columnIndex++);
            }
        } finally {
            cursor.close();
        }
        return user;
    }


    /**
     * method used to delete all records in user table
     */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from tableUsers");
        } catch (Exception e) {
            Log.d(TAG, "delete users failure: " + e.getMessage());
        }
    }


    /**
     * method used to add a tweet to the database, insert values
     *
     * @param tweet reference to tweet object to be added to database
     */
    public void addTweet(Tweet tweet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, tweet._id);
        values.put(TWEET_MESSAGE, tweet.message);
        values.put(DATE, tweet.createdAt);
        values.put(PHOTO, tweet.photo);
        values.put(BLOGGER, tweet.blogger.firstName);


        // Insert record
        db.insert(TABLE_TWEETS, null, values);
        db.close();
    }

    /**
     * method used to delete a specific record in tweet table
     *
     * @param tweet the tweet to be deleted
     */
    public void deleteTweet(Tweet tweet) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("tableTweets", "id" + "=?", new String[]{tweet._id + ""});
        } catch (Exception e) {
            Log.d(TAG, "delete tweet failure: " + e.getMessage());
        }
    }

    /**
     * Query database and select entire tableTweets.
     *
     * @return A list of Tweet object records
     */
    public List<Tweet> selectTweets() {
        List<Tweet> tweets = new ArrayList<Tweet>();
        String query = "SELECT * FROM " + "tableTweets";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int columnIndex = 0;
            do {
                Tweet tweet = new Tweet();
                tweet._id = cursor.getString(columnIndex++);
                tweet.message = cursor.getString(columnIndex++);
                tweet.createdAt = cursor.getString(columnIndex++);
                tweet.photo = cursor.getString(columnIndex++);
                tweet.blogger.firstName = cursor.getString(columnIndex++);

                columnIndex = 0;

                tweets.add(tweet);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tweets;
    }

    /**
     * method used to delete all records in the tweet table
     */
    public void deleteTweets() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from tableTweets");
        } catch (Exception e) {
            Log.d(TAG, "delete tweets failure: " + e.getMessage());
        }
    }

    /**
     * method is called when the db needs to be up graded
     * drops the tables within and creates a new
     *
     * @param db         the database in question
     * @param oldVersion the old version being dropped
     * @param newVersion the new version to be implemented
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_TWEETS);
        db.execSQL("drop table if exists " + TABLE_USERS);
        Log.d(TAG, "onUpdated");
        onCreate(db);
    }
}