package org.wit.mytweet.helpers;


import java.io.Serializable;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.NavUtils;

public class IntentHelper {
    /**
     * method used to start an activity while passing data to that activity
     *
     * @param parent    the parent activity to start the intent
     * @param classname the class name being implemented
     * @param extraID   id placed in activity
     * @param extraData the data you would like to pass through
     */
    public static void startActivityWithData(Activity parent, Class classname, String extraID, Serializable extraData) {
        Intent intent = new Intent(parent, classname);
        intent.putExtra(extraID, extraData);
        parent.startActivity(intent);
    }

    /**
     * method used to navigate back to specified parent
     *
     * @param parent the parent to navigate back to
     */
    public static void navigateUp(Activity parent) {
        Intent upIntent = NavUtils.getParentActivityIntent(parent);
        NavUtils.navigateUpTo(parent, upIntent);
    }


}