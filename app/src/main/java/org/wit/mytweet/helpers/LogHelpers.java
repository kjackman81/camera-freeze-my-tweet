package org.wit.mytweet.helpers;

import android.util.Log;

public class LogHelpers {
    /**
     * method used to provide an information message
     *
     * @param parent  the parent activity in question
     * @param message the message being displayed
     */
    public static void info(Object parent, String message) {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}