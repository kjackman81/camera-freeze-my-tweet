package org.wit.mytweet.helpers;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.content.ContentResolver;


public class ContactHelper {

    /**
     * method used to retrieve the email address form th contacts
     *
     * @param context in which  to retrieve the data
     * @param data    the data being retrieved
     * @return email the email address of the recipient if any
     */
    public static String getEmail(Context context, Intent data) {
        String email = "no email";

        Uri contactUri = data.getData();
        ContentResolver cr = context.getContentResolver();

        Cursor cur = cr.query(contactUri, null, null, null, null);

        if (cur.getCount() > 0) {
            try {
                cur.moveToFirst();
                String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor emails = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
                emails.moveToFirst();
                email = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                emails.close();
            } catch (Exception e) {
            }
        }
        return email;
    }

    /**
     * method used to retrieve the name form the contacts
     *
     * @param context in which to retrieve the data
     * @param data    the data being retrieved
     * @return contact the name of the contact
     */
    public static String getContact(Context context, Intent data) {
        String contact = "unable to find contact";
        Uri contactUri = data.getData();
        String[] queryFields = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
        Cursor c = context.getContentResolver().query(contactUri, queryFields, null, null, null);
        if (c.getCount() == 0) {
            c.close();
            return contact;
        }
        c.moveToFirst();
        contact = c.getString(0);
        c.close();

        return contact;
    }

    /**
     * method used to facilitate the sending of the mail
     * to the specified contact
     *
     * @param context in which the mail is sent
     * @param email   the emil address being sent to
     * @param subject the subject of the mail
     * @param body    the message in the email
     */
    public static void sendEmail(Context context, String email, String subject, String body) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(emailIntent, "Sending Email"));
    }

}