	


The intent of this project is to allow the user to simulate sending or small message or tweets.

app
~~~~~~~~~~~~~~~~~~~~


This directory contains the full implementation of a basic application for
the Android platform, demonstrating the basic facilities that the application
will use. 

The files contained here:


AndroidManifest.xml

This XML file describes to the Android platform what your application can do.
It is a required file, and is the mechanism you use to show your application
what is availacble to it ie entry point,activity list etc..


java/*

Under this directory is the Java source for for your application.

\mytweet\app\src\java

in here are the packages with the classes used for implementation
activities, app, helpers, models, settings and sqlite dbHelper


res/*

Under this directory are the resources for your application.

mytweet\app\src\main\res\

in here are the resourse used by the application such as the layouts, menues, 
drawable, values, xml mipmap


The res/drawable/ directory contains images and other things that can be
drawn to the screen.  These can be bitmaps (in .png or .jpeg format) or
special XML files describing more complex drawings.   Like layout files, the base name is used for the
resulting resource name.


res/values/colors.xml
res/values/strings.xml
res/values/styles.xml

These XML files describe additional resources included in the application.
They all use the same syntax; all of these resources could be defined in one
file, but we generally split them apart as shown here to keep things organized.